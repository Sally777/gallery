import React from 'react';
import {Route, Switch} from "react-router-dom";

import Register from "./containers/RegisterForm/RegisterForm";
import Login from "./containers/Login/Login";
import PhotoGallery from "./containers/PhotoGallery/PhotoGallery";
import PostForm from "./containers/PostForm/PostForm";
import UserGallery from "./containers/UserGallery/UserGallery";

const Routes = () => {
    return (
        <Switch>
            <Route path="/" exact component={PhotoGallery} />
            <Route path="/posts/new" exact component={PostForm} />
            <Route path="/users/:id" exact component={UserGallery} />
            <Route path="/register" exact component={Register}/>
            <Route path="/login" exact component={Login} />
        </Switch>
    );
};

export default Routes;