import axios from '../../axios-api';
import {NotificationManager} from "react-notifications";

export const FETCH_POSTS_SUCCESS = 'FETCH_POSTS_SUCCESS';
export const CREATE_POST_SUCCESS = 'CREATE_POST_SUCCESS';
export const REMOVE_POST_SUCCESS = 'REMOVE_POST_SUCCESS';

export const fetchPostsSuccess = posts => ({type: FETCH_POSTS_SUCCESS, posts});
export const createPostSuccess = () => ({type: CREATE_POST_SUCCESS});
export const removePostSuccess = () => ({type: REMOVE_POST_SUCCESS});

export const fetchPosts = postAuthor => {
    return dispatch => {
        let path = '/posts';

        if (postAuthor) {
            path += '?authorPosts=' + postAuthor;
        }
        return axios.get(path).then(
            response => dispatch(fetchPostsSuccess(response.data))
        );
    };
};

export const createPost = postData => {
    return dispatch => {
        return axios.post('/posts', postData).then(
            () => dispatch(createPostSuccess()),
             NotificationManager.success('You posted a photo! ')
        );
    };
};

export const removePost = postData => {
    return dispatch => {
        return axios.delete('/posts', postData).then(
            () => dispatch(removePostSuccess())
        )
    }
};