import React, {Component, Fragment} from 'react';
import {Button} from "reactstrap";
import {Link} from "react-router-dom";
import {connect} from "react-redux";

import {fetchPosts} from "../../store/actions/postsAction";
import PostListItem from "../../components/PostListItem/PostListItem";


class UserGallery extends Component {
    componentDidMount() {
        this.props.fetchPosts(this.props.match.params.id);
    }

    render() {
        return (
            <Fragment>
                <h2>{this.props.posts.post.author}'s gallery</h2>
                {this.props.user && this.props.user._id === this.props.posts.post.author &&
                <Link to="/posts/new">
                    <Button color="primary" className="float-right">
                        Add new photo
                    </Button>
                </Link>
                }
                {this.props.posts.map(post => (
                    <PostListItem
                        key={post._id}
                        _id={post._id}
                        title={post.title}
                        image={post.image}
                        author={post.author}
                    />
                ))}
            </Fragment>
        );
    }
}


const mapStateToProps = state => ({
    posts: state.posts.posts,
    user: state.users.user
});

const mapDispatchToProps = dispatch => ({
    fetchPosts: postAuthor => dispatch(fetchPosts(postAuthor)),
});

export default connect(mapStateToProps, mapDispatchToProps)(UserGallery);