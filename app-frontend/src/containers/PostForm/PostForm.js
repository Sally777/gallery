import React, {Component, Fragment} from 'react';
import NewPostForm from "../../components/NewPostForm/NewPostForm";
import {createPost} from "../../store/actions/postsAction";
import {connect} from "react-redux";

class PostForm extends Component {

    createPost = postData => {
        this.props.createPost(postData).then(() => {
            this.props.history.push('/');
        })
    };

    render() {
        return (
            <Fragment>
                <h2>Add new Post</h2>
                <NewPostForm
                    onSubmit={this.createPost}
                />
            </Fragment>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    createPost: postData => dispatch(createPost(postData))
});

export default connect(null, mapDispatchToProps)(PostForm);