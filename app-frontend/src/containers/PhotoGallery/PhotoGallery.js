import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";

import {fetchPosts} from "../../store/actions/postsAction";
import PostListItem from "../../components/PostListItem/PostListItem";
import {Button, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";

class PhotoGallery extends Component {

    state = {
        selectedPost: null
    };

    componentDidMount() {
        this.props.fetchPosts();
    }

    showModal = post  => {
        this.setState({selectedPost: post});
    };

    hideModal = () => {
        this.setState({selectedPost: null});
    };

    render() {
        return (
            <Fragment>
                <Modal isOpen={!!this.state.selectedPost} toggle={this.hideModal}>
                    {this.state.selectedPost && (
                        <Fragment>
                            <ModalHeader toggle={this.hideModal}>Full photo</ModalHeader>
                            <ModalBody>
                                <img src={this.state.selectedPost.image} style={{maxHeight: '300px', maxWidth: '300px', display: 'block'}} alt="Photo"/>
                            </ModalBody>
                            <ModalFooter>
                                <Button color="secondary" onClick={this.hideModal}>Close</Button>
                            </ModalFooter>
                        </Fragment>
                    )}
                </Modal>
                {this.props.posts.map(post => (
                    <PostListItem
                      key={post._id}
                      _id={post._id}
                      title={post.title}
                      image={post.image}
                      author={post.author}
                      onClick={() => this.showModal(post)}
                    />
                ))}
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    posts: state.posts.posts,
    user: state.users.user
});

const mapDispatchToProps = dispatch => ({
    fetchPosts: () => dispatch(fetchPosts()),
});

export default connect(mapStateToProps, mapDispatchToProps)(PhotoGallery);