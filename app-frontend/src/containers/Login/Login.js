import React, {Component, Fragment} from 'react';
import {Button, Form, FormGroup} from "reactstrap";
import FacebookLogin from "../../components/FacebookLogin/FacebookLogin";
import FormElement from "../../components/UI/FormElement/FormElement";
import {loginUser} from "../../store/actions/usersActions";

class Login extends Component {
    state = {
        username: '',
        password: ''
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };

    submitFormHandler = event => {
        event.preventDefault();

        this.props.loginUser({...this.state});
    };

    render() {
        return (
            <Fragment>
                <h2>Login</h2>
                <Form onSubmit={this.submitFormHandler}>
                    <FormGroup>
                        <FacebookLogin />
                    </FormGroup>
                    <FormElement
                        propertyName="username"
                        title="Username"
                        type="text"
                        value={this.state.username}
                        onChange={this.inputChangeHandler}
                        placeholder="Enter your username"
                    />

                    <FormElement
                        propertyName="password"
                        title="Password"
                        type="text"
                        value={this.state.password}
                        onChange={this.inputChangeHandler}
                        placeholder="Enter secure password"
                    />

                    <FormGroup row>
                        <Button type="submit" color="primary">Login</Button>
                    </FormGroup>
                </Form>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
   error: state.user.loginError
});

const mapDispatchToProps = dispatch => ({
    loginUser: userData => dispatch(loginUser(userData))
});

export default Login;