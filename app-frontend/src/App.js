import React, {Component, Fragment} from 'react';
import {NotificationContainer} from "react-notifications";
import {Container} from "reactstrap";
import Routes from "./Routes";
import Toolbar from "./components/UI/Toolbar/Toolbar";
import {logoutUser} from "./store/actions/usersActions";
import {connect} from "react-redux";
import {withRouter} from "react-router-dom";

class App extends Component {
  render() {
    return (
        <Fragment>
            <NotificationContainer/>
            <header>
                <Toolbar
                    user={this.props.user}
                    logout={this.props.logoutUser}/>
            </header>
            <Container style={{marginTop: '20px'}}>
                <Routes />
            </Container>
        </Fragment>
    );
  }
}

const mapStateToProps = state => ({
    user: state.users.user
});

const mapDispatchToProps = dispatch => ({
    logoutUser: () => dispatch(logoutUser())
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
