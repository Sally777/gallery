import React from 'react';
import {Card, CardBody, CardImg} from "reactstrap";
import {Link} from "react-router-dom";
import PropTypes from 'prop-types';

const PostListItem = props => {
    return (
        <Card style={{marginTop: '10px'}}>
           <CardImg src={props.image} top width="100%" alt="Photo" onClick={props.onClick}/>
           <CardBody>
               <p>{props.title}</p>
               <Link to={'/users/' + props.author}>By: {props.author}</Link>
           </CardBody>
        </Card>
    );
};

PostListItem.propsTypes = {
    image: PropTypes.string,
    title: PropTypes.string.isRequired,
    author: PropTypes.string.isRequired
};

export default PostListItem;