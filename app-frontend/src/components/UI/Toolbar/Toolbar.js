import React from 'react';
import NavItem from "reactstrap/lib/NavItem";
import {Nav, Navbar, NavbarBrand, NavLink} from "reactstrap";
import {NavLink as RouterNavLink} from 'react-router-dom';
import UserMenu from "./Menus/UserMenu";
import AnonymousMenu from "./Menus/AnonymousMenu";

const Toolbar = ({user, logout}) => {
    return (
        <Navbar color="light" light expand="md">
            <NavbarBrand tag={RouterNavLink} to="/">Photo Gallery</NavbarBrand>

            <Nav>
                <NavItem>
                    <NavLink tag={RouterNavLink} to="/" exact>Gallery</NavLink>
                </NavItem>
                {user ? <UserMenu user={user} logout={logout}/> : <AnonymousMenu/>}
            </Nav>
        </Navbar>
    );
};

export default Toolbar;