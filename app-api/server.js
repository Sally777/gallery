const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');

const config = require('./config');
const users = require('./app/users');
const posts = require('./app/posts');

const app = express();

app.use(express.json());
app.use(cors);

const port = 8000;

mongoose.connect(config.dbUrl, config.mongoOptions).then(() => {
    app.use('/users', users);
    app.use('/posts', posts);

    app.listen(port, () => {
        console.log(`Server started on ${port} port`);
    })
});