const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const config = require('../config');

const auth = require('../middleware/auth');
const Post = require('../models/Post');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', async(req, res) => {
    try {
        const criteria = {};

        if (req.query.authorPosts) {
            criteria.authorPosts = req.query.authorPosts;
        }

        const posts =  await Post.find(criteria);
        res.send(posts);
    }
    catch (e) {
        res.sendStatus(500);
    }
});

router.post('/', [auth, upload.single('image')], (req, res) => {
    const postData = req.body;

    const post = new Post(postData);

    post.save()
        .then(results => res.send(results))
        .catch(error => res.status(400).send(error));
});

router.delete('/', auth, async (req, res) => {
    await Post.findByIdAndDelete(req.body.id,
        function (err) {
            if (err) {
                console.log(err);
            }
            console.log('Successful deleted');
        }
    )});

module.exports = router;
