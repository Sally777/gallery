const mongoose = require('mongoose');
const config = require('./config');
const nanoid = require('nanoid');

const Post = require('./models/Post');
const User = require('./models/User');

const run = async () => {
    await mongoose.connect(config.dbUrl, config.mongoOptions);

    const connection = mongoose.connection;

    const collections = await connection.db.collections();

    for (let collection of collections) {
        await collection.drop();
    }

    const [author, author1, author2] =  await User.create({
        username: 'author',
        password: '777',
        token: nanoid()
    }, {
        username: 'author1',
        password: '333',
        token: nanoid()
    }, {
        username: 'author2',
        password: '333',
        token: nanoid()
    });

    await Post.create({
        title: 'Sky is blue',
        image: "sky.jpeg",
        author: author1._id
    }, {
        title: 'Worlds most beautiful creature',
        image: "creature.jpeg",
        author: author2._id
    }, {
        title: 'Happy memories',
        image: "memories.jpg",
        author: author2._id
    },  {
        title: 'Champagne bottle',
        image: "champagne.jpg",
        author: author._id
    }, {
        title: 'Pizza',
        image: "pizza_fresca.jpg",
        author: author._id
    }, {
        title: 'At work',
        image: "collegues.jpg",
        author: author2._id
    });
    return connection.close();
};

run().catch(error => {
    console.log(`Something wrong happened`, error);
});