const path = require('path');

const rootPath = __dirname;

module.exports = {
    rootPath,
    uploadPath: path.join(rootPath, 'public/uploads'),
    dbUrl: 'mongodb://localhost/app',
    mongoOptions: {
        useUnifiedTopology: true,
        useNewUrlParser: true,
        useCreateIndex: true
    },
    facebook: {
        appId: '1108412262844955',
        appSecret: 'e0dbad98e61fc8abdad8e399d445d08f'
    }
};

